import React from "react";
import moment from "moment";

export default function ItemMovieTheaterTabs({ data }) {
  return (
    <div className="p-3 flex space-x-5 border-b border-red-500">
      <img className="w-28 h-36 object-cover" src={data.hinhAnh} alt="123" />
      <div className="flex-grow">
        <div>
          <h2 className="text-xl">
            <span className="text-sm mr-2 bg-orange-500 rounded text-white px-2 py-1">
              C18
            </span>
            {data.tenPhim}
          </h2>
        </div>
        <div className="grid grid-cols-4 gap-5">
          {data.lstLichChieuTheoPhim.slice(0, 9).map((gioChieu) => {
            return (
              <div className="p-3 bg-transparent hover:bg-orange-500 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded text-center">
                {moment(gioChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm"
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
