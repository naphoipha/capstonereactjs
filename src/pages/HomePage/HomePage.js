import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer/Footer";
import MultipleRows from "../../components/Slick/MultipleRowsSlick";
import { moviesServ } from "../../services/moviesService";
import MovieTheaterTabs from "./MovieTheaterTabs";
import MyCarousel from "./MyCarousel";

export default function HomePage(props) {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    moviesServ
      .getListMovie()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <MyCarousel />
      <div className="container px-5 py-24 mx-auto">
        <MultipleRows movies={movies} />
        <div className="mt-24">
          <button class="mb-5 px-10 bg-transparent text-orange-500 font-semibold py-2 border border-orange-500 rounded">
            CỤM RẠP
          </button>
          <MovieTheaterTabs />
        </div>
      </div>
      <Footer />
    </div>
  );
}
