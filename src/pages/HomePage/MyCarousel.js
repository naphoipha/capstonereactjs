import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { moviesServ } from "../../services/moviesService";
import "./MyCarousel.css";

const contentStyle = {
  height: "600px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  backgroundPosition: "center",
  backgroundSize: "60%",
  backgroundRepeat: "no-repeat",
};

export default function MyCarousel() {
  const [banner, setBanner] = useState([]);
  useEffect(() => {
    moviesServ
      .getBannerList()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  const renderBanner = () => {
    return banner.map((item, index) => {
      return (
        <div key={index}>
          <div
            style={{
              ...contentStyle,
              backgroundImage: `url(${item.hinhAnh})`,
            }}
          >
            <img
              className="w-full opacity-0"
              src={item.hinhAnh}
              alt={item.maPhim}
            />
          </div>
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderBanner()}</Carousel>;
}
