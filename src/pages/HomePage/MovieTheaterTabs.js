import React, { useEffect, useState } from "react";
import { moviesServ } from "../../services/moviesService";
import ItemMovieTheaterTabs from "./ItemMovieTheaterTabs";
import { Tabs } from "antd";

export default function MovieTheaterTabs() {
  const [dataTheater, setDataTheater] = useState([]);
  useEffect(() => {
    moviesServ
      .getMovieTheater()
      .then((res) => {
        setDataTheater(res.data.content);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, []);

  let renderContent = () => {
    return dataTheater.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <img className="w-16 h-16" src={heThongRap.logo} alt="logo rap" />
          }
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left">
                      <p className="text-green-700 truncate font-bold">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate text-gray-500">{cumRap.diaChi}</p>
                      <p className="text-red-500">[chi tiết...]</p>
                    </div>
                  }
                  key={index}
                >
                  <div style={{ height: 500, overflowY: "scroll" }}>
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemMovieTheaterTabs data={phim} />;
                    })}
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
