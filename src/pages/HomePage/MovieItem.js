import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function MovieItem({ data = {} }) {
  return (
    <div className="ml-2 h-full">
      <Card
        hoverable
        style={{
          width: "100%",
          borderRadius: "5px",
          overflow: "hidden",
        }}
        cover={
          <img
            className="h-80 w-full object-cover"
            alt={data.tenPhim}
            src={data.hinhAnh}
          />
        }
      >
        <Meta
          style={{ color: "gray", fontWeight: "bold" }}
          className="text-gray-500"
          title={<p className="truncate">{data.tenPhim}</p>}
          description={<p className="truncate">{data.moTa}</p>}
        />
        <NavLink to={`/detail/${data.maPhim}`}>
          {/* <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
            Đặt Vé
          </button>
          <button className="ml-3 bg-transparent hover:bg-orange-500 text-orange-700 font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded">
            Lịch Chiếu
          </button> */}
        </NavLink>
      </Card>
    </div>
  );
}
