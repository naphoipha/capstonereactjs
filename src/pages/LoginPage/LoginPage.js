import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { localService } from "../../services/localService";
import { SET_USER } from "../../redux/constants/constantUser";
import { userServ } from "../../services/userService";
import { Button, Form, Input, message, Checkbox } from "antd";
import { NavLink } from "react-router-dom";
import "./LoginPage.css";

export default function LoginPage() {
  let navigate = useNavigate();

  let dispatch = useDispatch();

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        //lưu vào local storage
        localService.user.set(res.data.content);

        //dispatch to store
        dispatch({
          type: SET_USER,
          payload: res.data.content,
        });

        //chuyển hướng sang trang chủ
        message.success("Đăng nhập thành công");
        setTimeout(() => {
          navigate("/");
        }, 2000);

        console.log(res);
      })
      .catch((err) => {
        message.success("Đăng nhập thất bại");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="mx-auto flex items-center justify-center login-bg">
      {/* <div className="w-1/2 h-full flex items-center justify-center">
        <Lottie animationData={bgAnimate} />
      </div> */}
      <div className=" h-full flex items-center justify-center login-box">
        <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-14 h-14 login-logo"
            >
              <path
                fillRule="evenodd"
                d="M18.685 19.097A9.723 9.723 0 0021.75 12c0-5.385-4.365-9.75-9.75-9.75S2.25 6.615 2.25 12a9.723 9.723 0 003.065 7.097A9.716 9.716 0 0012 21.75a9.716 9.716 0 006.685-2.653zm-12.54-1.285A7.486 7.486 0 0112 15a7.486 7.486 0 015.855 2.812A8.224 8.224 0 0112 20.25a8.224 8.224 0 01-5.855-2.438zM15.75 9a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z"
                clipRule="evenodd"
              />
            </svg>
          </Form.Item>
          <Form.Item className="login-title">Đăng Nhập</Form.Item>
          <Form.Item name="username" required>
            <Input placeholder="Tài Khoản"></Input>
          </Form.Item>
          <Form.Item name="password" required>
            <Input placeholder="Password"></Input>
          </Form.Item>
          <Form.Item>
            <Button block type="primary" htmlType="submit">
              Đăng Nhập
            </Button>
          </Form.Item>
          <Form.Item>
            <NavLink>Quên Mật Khẩu?</NavLink>
            <NavLink to="/signup" className="ml-3">
              Chưa có tài khoản? Đăng Kí
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
