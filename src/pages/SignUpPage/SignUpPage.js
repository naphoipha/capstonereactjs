import React from "react";
import "./SignUpPage.css";
import { Button, Form, Input, message, Checkbox } from "antd";
import { NavLink } from "react-router-dom";

export default function SignUpPage() {
  return (
    <div className="mx-auto flex items-center justify-center signup-bg">
      {/* <div className="w-1/2 h-full flex items-center justify-center">
    <Lottie animationData={bgAnimate} />
  </div> */}
      <div className="flex items-center justify-center signup-box">
        <Form>
          <Form.Item>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-14 h-14 signup-logo"
            >
              <path
                fillRule="evenodd"
                d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                clipRule="evenodd"
              />
            </svg>
          </Form.Item>
          <Form.Item className="signup-title">Đăng Ký</Form.Item>
          <Form.Item name="username" required>
            <Input placeholder="Tên Tài Khoản"></Input>
          </Form.Item>
          <Form.Item name="password" required>
            <Input placeholder="Mật Khẩu"></Input>
          </Form.Item>
          <Form.Item name="" required>
            <Input placeholder="Nhập lại mật khẩu"></Input>
          </Form.Item>
          <Form.Item name="name" required>
            <Input placeholder="Họ và tên"></Input>
          </Form.Item>
          <Form.Item name="email" required>
            <Input placeholder="Email"></Input>
          </Form.Item>
          <Form.Item>
            <Button block type="primary" htmlType="submit">
              Đăng Ký
            </Button>
          </Form.Item>
          <Form.Item>
            <NavLink to="/login" className="ml-3">
              Đã Có Tài Khoản? Đăng Nhập
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
