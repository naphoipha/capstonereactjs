import React, { useState } from "react";
import { useSelector } from "react-redux";
import AddUser from "./AddUser";
import UserList from "./UserList";

export default function UserManegement() {
  const userView = useSelector((state) => state.userViewSlice.view);
  const viewToRender = {
    1: <UserList />,
    2: <AddUser />,
    3: <UserList />,
    4: <UserList />,
  };
  return <>{viewToRender[userView]}</>;
}
