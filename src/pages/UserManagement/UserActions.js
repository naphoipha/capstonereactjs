import { Button, message } from "antd";
import React, { useState } from "react";
import { localServ } from "~/services/localService";
import { userServ } from "../../services/userService";
import { useDispatch } from "react-redux";
import { setModal } from "~/redux/slice/userSlice";

const EDIT_BTN = "edit_btn";
const DELETE_BTN = "delete_btn";

export default function UserActions({
  taiKhoan,
  onSuccess,
  openModal,
  buttonIndex,
}) {
  let [loadings, setLoadings] = useState({ [DELETE_BTN]: [], [EDIT_BTN]: [] });

  let enterLoading = (buttonIndex, buttonType) => {
    setLoadings((prev) => {
      const nextState = { ...prev };
      nextState[buttonType][buttonIndex] = true;
      return nextState;
    });
  };

  let quitLoading = (buttonIndex, buttonType) => {
    setLoadings((prev) => {
      const nextState = { ...prev };
      nextState[buttonType][buttonIndex] = false;
      return nextState;
    });
  };

  let handleDeleteUser = (buttonIndex) => {
    enterLoading(buttonIndex, DELETE_BTN);
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xóa user thành công");
        quitLoading(buttonIndex, DELETE_BTN);
        onSuccess();
      })
      .catch((err) => {
        quitLoading(buttonIndex, DELETE_BTN);
        message.error(err.response.data.content);
      });
  };

  let handleEditUser = (buttonIndex) => {
    enterLoading(buttonIndex, EDIT_BTN);
    userServ
      .getUserInfo(taiKhoan)
      .then((res) => {
        quitLoading(buttonIndex, EDIT_BTN);
        openModal(res.data.content);
        console.log(res.data.content);
      })
      .catch((err) => {
        quitLoading(buttonIndex, EDIT_BTN);
        message.error(err.message);
        console.log(err);
      });
  };
  return (
    <div className="space-x-2">
      <Button
        loading={loadings[DELETE_BTN][buttonIndex]}
        type="danger"
        onClick={() => {
          handleDeleteUser(buttonIndex);
        }}
        className="px-5 py-1 rounded bg-red-400 text-white font-medium"
        icon={<div>Xóa</div>}
      >
        <div className=""></div>
      </Button>
      <Button
        type="primary"
        loading={loadings[EDIT_BTN][buttonIndex]}
        onClick={() => {
          handleEditUser(buttonIndex);
        }}
        icon={<div>Sửa</div>}
      >
        <div className=""></div>
      </Button>
    </div>
  );
}
