import { Button, Divider, message } from "antd";
import React, { useEffect, useState } from "react";
import { localServ } from "~/services/localService";
import { controller, userServ } from "../../services/userService";
import UserActions from "./UserActions";
import UserEditModal from "./UserEditModal";
import UserTable from "./UserTable";
import { UserAddOutlined } from "@ant-design/icons";

const modalInitState = { open: false, data: {} };

export default function UserList() {
  const [userList, setUserList] = useState([]);
  let [modalState, setModalState] = useState(modalInitState);
  let openModal = (userInfo) => {
    setModalState({ open: true, data: userInfo });
  };
  let closeModal = () => {
    setModalState(false);
  };
  let fetchUserList = () => {
    userServ
      .getUserList()
      .then((res) => {
        let taiKhoanAdmin = localServ.user.get().taiKhoan;
        let filterAdminData = res.data.content.filter(
          (x) => x.taiKhoan !== taiKhoanAdmin
        );
        let data = filterAdminData.map((item, index) => {
          return {
            ...item,
            action: (
              <UserActions
                buttonIndex={index}
                openModal={openModal}
                onSuccess={fetchUserList}
                taiKhoan={item.taiKhoan}
              />
            ),
          };
        });
        setUserList(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    fetchUserList();
  }, []);
  return (
    <div className=" mx-auto  bg-white rounded-lg">
      <UserEditModal
        fetchUserList={fetchUserList}
        modalState={modalState}
        closeModal={closeModal}
      />
      <div className="px-3 py-4 font-serif font-normal text-base flex">
        <p className="mb-0 text-lg">Danh sách User</p>
      </div>
      <div className="px-1">
        <UserTable userList={userList} />
      </div>
    </div>
  );
}
