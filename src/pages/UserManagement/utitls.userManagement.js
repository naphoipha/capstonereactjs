import { Tag } from "antd";

export const headColumns = [
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Người dùng",
    dataIndex: "hoTen",
    key: "hoten",
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taikhoan",
  },
  {
    title: "Loại tài khoản",
    dataIndex: "maLoaiNguoiDung",
    key: "maloainguoidung",
    render: (text) => {
      if (text == "QuanTri") {
        return <Tag color="volcano"> Quản Trị </Tag>;
      } else {
        return <Tag color="green"> Người dùng </Tag>;
      }
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
  },
];

// email: "e1e1212qwdwqqd1212";
// hoTen: "2e1e112";
// maLoaiNguoiDung: "KhachHang";
// matKhau: "e21e21e1";
// soDT: "12e12";
// taiKhoan: "21e12e22121212122";
