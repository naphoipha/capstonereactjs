import { Button, Modal } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import UserInputForm from "./UserInputForm";

const UserAddModal = ({ modalState, closeModal, fetchUserList }) => {
  const [confirmLoading, setConfirmLoading] = useState(false);
  let [userInputInfo, setUserInfo] = useState({});

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      closeModal();
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    console.log("Clicked cancel button");
    closeModal();
    // setOpen(false);
  };

  console.log("User Edit Modal Render");

  return (
    <>
      <Modal
        title={
          <div className="flex justify-center">
            Cật nhật thông tin tài khoản
          </div>
        }
        open={modalState.open}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        footer={null}
      >
        <UserInputForm
          fetchUserList={fetchUserList}
          closeModal={closeModal}
          userInfo={modalState.data}
        />
      </Modal>
    </>
  );
};

export default UserAddModal;
