import { Space, Table, Tag } from "antd";
import React, { useEffect } from "react";
import { headColumns } from "./utitls.userManagement";

const UserTable = ({ userList }) => {
  useEffect(() => {});
  return (
    <Table
      rowKey={(record) => record.taiKhoan}
      pagination={{
        position: ["bottomCenter"],
      }}
      columns={headColumns}
      dataSource={userList}
    />
  );
};

export default UserTable;
