const { createSlice } = require("@reduxjs/toolkit");

const initState = {
  view: 1,
};

const userViewSlice = createSlice({
  name: "userViewSlice",
  initialState: initState,
  reducers: {
    setView: (state, action) => {
      state.view = action.payload;
    },
  },
});

export const { setView } = userViewSlice.actions;

export default userViewSlice.reducer;
