const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  isLogin: true,
  user: null,
  modalOpen: false,
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.user = action.payload;
    },
    setModal: (state, action) => {
      state.modalOpen = action.payload;
    },
  },
});

export const { setUserInfor, setModal } = userSlice.actions;

export default userSlice.reducer;
