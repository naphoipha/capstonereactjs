import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localService } from "../../services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogout = () => {
    localService.user.remove();
    window.location.href = "/login";
  };

  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="text-orange-500 hover:underline cursor-pointer transition font-bold">
            {user.hoTen}
          </span>
          <button
            onClick={handleLogout}
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Đăng Xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="font-semibold hover:text-orange-500 px-8 py-3 rounded text-black">
              Đăng Nhập
            </button>
          </NavLink>
          <NavLink to="/signup">
            <button className="bg-transparent hover:bg-orange-500 text-orange-500 font-semibold hover:text-white py-2 px-4 border border-orange-500 hover:border-transparent rounded">
              Đăng Kí
            </button>
          </NavLink>
        </>
      );
    }
  };

  return (
    <div className="items-center flex-shrink-0 hidden lg:flex">
      {renderContent()}
    </div>
  );
}
