import React from "react";
import "./FlipCard.css";
import { NavLink } from "react-router-dom";

export default function FlipCard(props) {
  const { data } = props;

  return (
    <div className="flip-card mt-2">
      <div className="flip-card-inner">
        <div className="flip-card-front">
          <div>
            <img
              src={data.hinhAnh}
              alt="Avatar"
              style={{ width: 300, height: 300 }}
            />
          </div>
        </div>
        <div
          className="flip-card-back"
          style={{
            position: "relative",
            backgroundColor: "rgba(0,0,0,.9)",
          }}
        >
          <div style={{ position: "absolute", top: 0, left: 0 }}>
            <img
              src={data.hinhAnh}
              alt="Avatar"
              style={{ width: 300, height: 300 }}
            />
          </div>
          <div
            className="w-full h-full"
            style={{
              width: 300,
              height: 300,
              position: "absolute",
              backgroundColor: "rgba(0,0,0,.5)",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <div>
              <div>
                <button className="bg-transparent hover:bg-red-500 text-white font-semibold hover:text-white py-2 px-4 border-2 border-red-500 hover:border-transparent rounded w-full">
                  Đặt Vé
                </button>
                <br />
                <NavLink to="/detail">
                  <button className="mt-2 bg-transparent hover:bg-orange-500 text-white font-semibold hover:text-white py-2 px-4 border-2 border-orange-500 hover:border-transparent rounded w-full">
                    Xem Chi Tiết
                  </button>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
