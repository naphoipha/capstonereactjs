import {
  AppstoreOutlined,
  ContainerOutlined,
  DesktopOutlined,
  MailOutlined,
  UserOutlined,
  YoutubeOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
  OrderedListOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { Button, Menu } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setView } from "~/redux/slice/userViewSlice";

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items = [
  getItem("Quan ly User", "sub1", <UserOutlined />, [
    getItem("Danh sach User", "1", <OrderedListOutlined />),
    getItem("Them User", "2", <UserAddOutlined />),
  ]),
  getItem("Quan ly Movie", "sub2", <YoutubeOutlined />, [
    getItem("Danh sach User", "3"),
    getItem("Them User", "4"),
  ]),
  //   getItem("Option 3", "3", <ContainerOutlined />),
];

const SideMenu = () => {
  const [collapsed, setCollapsed] = useState(false);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  let dispatch = useDispatch();

  const onClick = (e) => {
    dispatch(setView(e.key * 1));
  };
  return (
    <div>
      {/* <Button
        type="primary"
        onClick={toggleCollapsed}
        style={{
          marginBottom: 16,
        }}
      >
        {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </Button> */}
      <Menu
        onClick={onClick}
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        theme="light"
        inlineCollapsed={collapsed}
        items={items}
      />
    </div>
  );
};

export default SideMenu;
