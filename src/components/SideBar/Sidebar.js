import React from "react";
import styles from "./Sidebar.module.scss";
import clsx from "clsx";
import SideMenu from "./SideMenu";

export default function Sidebar() {
  return (
    <div className={clsx(styles.content)}>
      <div className="font-medium py-5 pl-6">Dashboard</div>
      <SideMenu />
    </div>
  );
}
