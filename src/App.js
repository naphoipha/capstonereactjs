import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Layout from "./layout/Layout";
import HomePage from "./pages/HomePage/HomePage";
import "antd/dist/antd.css";
import LoginPage from "./pages/LoginPage/LoginPage";
import SignUpPage from "./pages/SignUpPage/SignUpPage";
import MultipleRows from "./components/Slick/MultipleRowsSlick";
import DetailMovie from "./pages/DetailMovie/DetailMovie";
import UserManagement from "./pages/UserManagement/UserManegement";
import MovieManagement from "./pages/MovieManagement/MovieManagement";
import Layout from "./HOC/Layout";
import SecureView from "./HOC/SecureView";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/" element={<Navigate to="/usermanagement" />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route path="/signup" element={<Layout Component={SignUpPage} />} />
          <Route path="/detail" element={<Layout Component={DetailMovie} />} />
          <Route
            path="/usermanagement"
            element={
              <SecureView>
                <Layout Component={UserManagement} />
              </SecureView>
            }
          />
          <Route
            path="/moviemanagement"
            element={
              <SecureView>
                <Layout Component={MovieManagement} />
              </SecureView>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
